package kz.astana.asynktaskexample;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private TextView progressTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressBar);
        progressTextView = findViewById(R.id.progressTextView);

        TextView countTextView = findViewById(R.id.countTextView);
        Button countButton = findViewById(R.id.countButton);
        countButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(countTextView.getText().toString());
                countTextView.setText(count + 1 + "");
            }
        });
        Button resetButton = findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countTextView.setText("0");
            }
        });

        Button uiThread = findViewById(R.id.runOnUiThread);
        Button backgroundThread = findViewById(R.id.runOnBackgroundThread);
        Button asynkTask = findViewById(R.id.runOnAsynkTask);

        uiThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread();
            }
        });

        backgroundThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnBackgroundThread();
            }
        });

        asynkTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MyAsynkTask().execute();
            }
        });

        Button openDownloadActivity = findViewById(R.id.openDownloadActivity);
        openDownloadActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, DownloadActivity.class));
            }
        });
    }

    private void runOnUiThread() {
        try {
            for (int i = 1; i <= 100; i++) {
                Thread.sleep(100);
                progressBar.setProgress(i);
                progressTextView.setText(progressBar.getProgress() + "%");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void runOnBackgroundThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 1; i <= 100; i++) {
                        Thread.sleep(100);
                        int finalI = i;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setProgress(finalI);
                                progressTextView.setText(progressBar.getProgress() + "%");
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private class MyAsynkTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setProgress(0);
            progressTextView.setText(progressBar.getProgress() + "%");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (int i = 1; i <= 100; i++) {
                    Thread.sleep(100);
                    publishProgress(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int i = values[0];
            progressBar.setProgress(i);
            progressTextView.setText(progressBar.getProgress() + "%");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}